companies
=========

|   name    |       email       | logo | website  |     created_at      |     Updated_at      |
|-----------|-------------------|------|----------|---------------------|---------------------|
| Company 1 | company1@test.com |      |          | 2021-07-12 02:33:57 | 2021-07-12 02:33:57 |
| Company 2 | company2@test.com |      | test.org | 2021-07-12 02:33:57 | 2021-07-12 02:33:57 |
| Company 3 | company3@test.com |      | omg.org  | 2021-07-12 02:33:57 | 2021-07-12 02:33:57 |
(3 rows)

