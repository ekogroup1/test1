<?php

use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;
use Illuminate\Database\Seeder;

class MySpreadsheetSeederClass extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->worksheetTableMapping = ['companies' => 'companies', 'employees' => 'employees'];
        
        parent::run();
    }
}
