<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use App\Http\Requests\StoreEmployee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('jwt.verify');
    }

    public function getListCompany(){
        $list_company = Company::select('id', 'name')->get();
        return Response::json($list_company);
    }

    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Employee::select('employees.id AS id', 'employees.first_name AS first_name', 'employees.last_name AS last_name', 'companies.name AS company_name', 'employees.email AS email','employees.phone AS phone', 'employees.created_at AS created_at', 'employees.updated_at AS updated_at')
                ->join('companies', 'employees.company_id', '=', 'companies.id')
            )
            ->addColumn('action', 'admin.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('admin/employee');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployee $request)
    {
        $request->validated();
        
        $employee_id = $request->employee_id;
            
        $employee = Employee::updateOrCreate(['id' => $employee_id],
            ['first_name' => $request->first_name,  'last_name' => $request->last_name, 'company_id' => $request->company_id, 'email' => $request->email, 'phone' => $request->phone
        ]);
    
        return Response::json($employee);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $employee = Employee::where('id', $id)->first();
    
        return Response::json($employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::where('id', $id)->delete();
  
        return Response::json($employee);
    }
}
