<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\StoreCompany;
use App\Jobs\SendNewCompanyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('jwt.verify');
    }

    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of(Company::select('id', 'name', 'email', 'logo','website', 'created_at', 'updated_at'))
            ->addColumn('action', 'admin.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('admin/company');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompany $request)
    {
        $request->validated();

        $is_logo = $request->has('logo');
        $filename = null;
        $company_id = $request->company_id;
            
        if($is_logo){
            $filename = $request->logo->hashName();
            Storage::putFile('public/inventory', $request->file('logo'));

            $company   = Company::updateOrCreate(['id' => $company_id],
                ['name' => $request->name, 'email' => $request->email, 'logo' => $filename,'website' => $request->website
            ]);
        }else{
            $company   = Company::updateOrCreate(['id' => $company_id],
                ['name' => $request->name, 'email' => $request->email,'website' => $request->website
            ]);
        }
        
        if($company->wasRecentlyCreated){
            if($request->email){
                SendNewCompanyEmail::dispatch($request->email, $request->name);
            }
        }

        return Response::json($company);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company  = Company::where('id', $id)->first();
    
        return Response::json($company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::where('id', $id)->delete();
  
        return Response::json($company);
    }
}
