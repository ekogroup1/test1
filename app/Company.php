<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //eko test 1
    protected $fillable = ['id', 'name', 'email', 'logo','website'];

}
