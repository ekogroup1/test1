<?php

namespace Tests\Unit;

use App\Company;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;

    /** @test */
    public function only_logged_in_user_can_access_company_menu()
    {
        $response = $this->get('/company')->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_access_company_menu()
    {
        $this->actingAs(factory(User::class)->create());
        $response = $this->get('/company')->assertOk();
    }

    /** @test */
    public function a_company_can_be_added_and_updated()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->post('/company', [
            'name' => 'Company A',
            'email' => 'companya@test.com',
            'website' => 'www.companya.com',
        ]);

        $this->assertCount(1, Company::all());

        $id = $response->decodeResponseJson()['id'];

        $update_response = $this->post('/company', [
            'company_id' => $id,
            'name' => 'Company A updated',
            'email' => 'companya@test.com',
            'website' => 'www.companya.com',
        ]);

        $this->assertCount(1, Company::all());
    }

    /** @test */
    public function delete_a_company()
    {
        $this->actingAs(factory(User::class)->create());

        $response = $this->post('/company', [
            'name' => 'Company A',
        ]);

        $this->assertCount(1, Company::all());

        $id = $response->decodeResponseJson()['id'];

        $this->delete('/company/'. $id);

        $this->assertCount(0, Company::all());
    }
}
