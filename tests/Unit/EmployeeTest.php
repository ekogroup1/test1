<?php

namespace Tests\Unit;

use App\Company;
use App\Employee;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    use RefreshDatabase;

    /** @test */
    public function only_logged_in_user_can_access_employee_menu()
    {
        $response = $this->get('/employee')->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_access_employee_menu()
    {
        $this->actingAs(factory(User::class)->create());
        $response = $this->get('/employee')->assertOk();
    }

    /** @test */
    public function a_employee_can_be_added_and_updated()
    {
        $this->actingAs(factory(User::class)->create());

        $response_company = $this->post('/company', [
            'name' => 'Company A',
            'email' => 'companya@test.com',
            'website' => 'www.companya.com',
        ]);

        $this->assertCount(1, Company::all());

        $company_id = $response_company->decodeResponseJson()['id'];

        $response_emloyee = $this->post('/employee', [
            'first_name' => 'Employee 1',
            'last_name' => 'employee1@test.com',
            'company_id' => $company_id,
        ]);

        $this->assertCount(1, Employee::all());

        $employee_id = $response_emloyee->decodeResponseJson()['id'];

        $this->post('/employee', [
            'employee_id' => $employee_id,
            'first_name' => 'Employee 1 updated',
            'last_name' => 'employee1updated@test.com',
        ]);

        $this->assertCount(1, Employee::all());
    }

    /** @test */
    public function delete_a_employee()
    {
        $this->actingAs(factory(User::class)->create());

        $response_company = $this->post('/company', [
            'name' => 'Company A',
            'email' => 'companya@test.com',
            'website' => 'www.companya.com',
        ]);

        $this->assertCount(1, Company::all());

        $company_id = $response_company->decodeResponseJson()['id'];

        $response_emloyee = $this->post('/employee', [
            'first_name' => 'Employee 1',
            'last_name' => 'employee1@test.com',
            'company_id' => $company_id,
        ]);

        $this->assertCount(1, Employee::all());

        $employee_id = $response_emloyee->decodeResponseJson()['id'];

        $this->delete('/employee/'. $employee_id);

        $this->assertCount(0, Employee::all());
    }
}
