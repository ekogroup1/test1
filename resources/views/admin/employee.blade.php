@extends('admin.admin')
@section('content')

<a href="javascript:void(0)" class="btn btn-info ml-3" id="create-new-employee">Add New</a>
<br><br>
<table id="table_employee" class="display">
    <thead>
        <tr>
            <th>id</th>
            <th>No</th>
            <th>{{ __('employee.first_name') }}</th>
            <th>{{ __('employee.last_name') }}</th>
            <th>{{ __('employee.company') }}</th>
            <th>Email</th>
            <th>Website</th>
            <th>{{ __('employee.created_at') }}</th>
            <th>{{ __('employee.updated_at') }}</th>
            <th>{{ __('employee.action') }}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            
        </tr>
        <tr>
            
            
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th>id</th>
            <th></th>
            <th>{{ __('employee.first_name') }}</th>
            <th>{{ __('employee.last_name') }}</th>
            <th>{{ __('employee.company') }}</th>
            <th>Email</th>
            <th>Website</th>
            <th>{{ __('company.created_at') }}</th>
            <th>{{ __('company.updated_at') }}</th>
            <th></th>
        </tr>
    </tfoot>
</table>

  
<div class="modal fade" id="ajax-employee-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="employeeCrudModal"></h4>
            </div>
            <div class="modal-body">
                <form id="employeeForm" name="employeeForm" class="form-horizontal" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
                <input type="hidden" name="employee_id" id="employee_id">
                    <div class="form-group">
                        <label for="first_name" class="col-sm-4 control-label">First Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name" value="" maxlength="50">
                        </div>
                        <span id="first_name_validation_message" style="color:red"></span>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-sm-4 control-label">Last Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name" value="" maxlength="50">
                        </div>
                        <span id="last_name_validation_message" style="color:red"></span>
                    </div>
                    <div class="form-group">
                        <label for="list_company" class="col-sm-4 control-label">Company</label>
                        <div class="col-sm-12">
                            <select class="form-control" id="list_company" name="company_id">
                                
                            </select>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-2 control-label">Phone</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter Phone" value="" maxlength="50">
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
                    </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('script1')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>
    // Source https://w3path.com/laravel-6-ajax-crud-tutorial-using-datatables-from-scratch/
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Setup - add a text input to each footer cell
        $('#table_employee tfoot th').each( function (index) {
            var title = $(this).text();
            if(index == 1 || index == 9){
                return;
            }
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );

        $('#table_employee').DataTable({
            "scrollX": true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "/employee",
                type: 'GET',
            },
            columns: [
                    {data: 'id', name: 'id', 'visible': false},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                    {data: 'first_name', name: 'first_name' },
                    {data: 'last_name', name: 'last_name' },
                    {data: 'company_name', name: 'companies.name'},
                    {data: 'email', name: 'email' },
                    {data: 'phone', name: 'phone' },
                    {data: 'created_at', name: 'created_at' },
                    {data: 'updated_at', name: 'updated_at' },
                    {data: 'action', name: 'action', orderable: false},
                ],
            order: [[0, 'desc']],
            initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;
    
                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
            }
        });

        /*  When user click add user button */
        $('#create-new-employee').click(function () {

            $('#btn-save').val("create-product");
            $('#employee_id').val('');
            $('#employeeForm').trigger("reset");
            $('#employeeCrudModal').html("Add New Employee");
            $('#ajax-employee-modal').modal('show');

            getListCompany();
            clearValidationMessage();
        });

        function getListCompany(){
            $.getJSON("get-list-company", function(data) {
                $("#list_company").html('');
                $.each(data, function(key, value){
                    $("#list_company").append('<option value="'+ value.id +'">'+ value.name +'</option>');
                });
            });
        }

        function clearValidationMessage(){
            $('#first_name_validation_message').html('');
            $('#last_name_validation_message').html('');
        }

        $( "#employeeForm" ).submit(function( event ) {
            $.ajax({
                // data: $('#employeeForm').serialize(),
                data: new FormData(this),
                url: "employee",
                type: "POST",
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#employeeForm').trigger("reset");
                    $('#ajax-employee-modal').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#table_employee').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    if(data.status == 422){
                        console.log('ini eko');
                        if(data.responseJSON.errors.first_name){
                            $('#first_name_validation_message').html(data.responseJSON.errors.first_name[0]);
                        }
                        if(data.responseJSON.errors.last_name){
                            $('#last_name_validation_message').html(data.responseJSON.errors.last_name[0]);
                        }
                    }
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            event.preventDefault();
        });

        /* When click edit user */
        $('body').on('click', '.edit-product', function () {
            
            getListCompany();
            clearValidationMessage();

            var employee_id = $(this).data('id');
            $.get('employee/' + employee_id +'/edit', function (data) {
                console.log('asd');
                // $('#title-error').hide();
                // $('#product_code-error').hide();
                // $('#description-error').hide();
                $('#employeeCrudModal').html("Edit Company");
                $('#btn-save').val("edit-employee");
                $('#ajax-employee-modal').modal('show');
                $('#employee_id').val(data.id);
                $('#first_name').val(data.first_name);
                $('#last_name').val(data.last_name);
                $('#list_company').val(data.company_id);
                $('#email').val(data.email);
                $('#phone').val(data.phone);
            })
        });

        $('body').on('click', '#delete-product', function () {
            var employee_id = $(this).data("id");
            var get_csrf_token = $('#csrf_token').val();

            if(confirm("Are You sure want to delete !")){
                $.ajax({
                    type: "DELETE",
                    data: {
                        _token: get_csrf_token
                    },
                    url: "employee/" + employee_id,
                    success: function (data) {
                        var oTable = $('#table_employee').dataTable(); 
                        oTable.fnDraw(false);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }); 
    });

    
</script>
    
@endsection
