@extends('admin.admin')
@section('content')

<a href="javascript:void(0)" class="btn btn-info ml-3" id="create-new-company">Add New</a>
<br><br>
<table id="table_company" class="display">
    <thead>
        <tr>
            <th>id</th>
            <th>No</th>
            <th>{{ __('company.name') }}</th>
            <th>Email</th>
            <th>Logo</th>
            <th>Website</th>
            <th>{{ __('company.created_at') }}</th>
            <th>{{ __('company.updated_at') }}</th>
            <th>{{ __('company.action') }}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            
        </tr>
        <tr>
            
            
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th>id</th>
            <th></th>
            <th>{{ __('company.name') }}</th>
            <th>Email</th>
            <th></th>
            <th>Website</th>
            <th>{{ __('company.created_at') }}</th>
            <th>{{ __('company.updated_at') }}</th>
            <th></th>
        </tr>
    </tfoot>
</table>

  
<div class="modal fade" id="ajax-company-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="companyCrudModal"></h4>
            </div>
            <div class="modal-body">
                <form id="companyForm" name="companyForm" class="form-horizontal" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
                <input type="hidden" name="company_id" id="company_id">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50">
                        </div>
                        <span id="name_validation_message" style="color:red"></span>
                    </div> 
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="" maxlength="50">
                        </div>
                    </div>
        
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Logo</label>
                        <div class="col-sm-12">
                            <input type="file" class="form-control-file" id="logo" name="logo" placeholder="Enter Logo" value="">
                        </div>
                        <span id="logo_validation_message" style="color:red"></span>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Website</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="website" name="website" placeholder="Enter Website" value="" maxlength="50">
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
                    </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('script1')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>
    // Source https://w3path.com/laravel-6-ajax-crud-tutorial-using-datatables-from-scratch/
    $(document).ready(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Setup - add a text input to each footer cell
        $('#table_company tfoot th').each( function (index) {
            var title = $(this).text();
            
            //Remove No, logo and Edit column, since not used for filter
            if(index == 1 || index == 4 ||index == 8){
                return;
            }
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );

        $('#table_company').DataTable({
            "scrollX": true,
            processing: true,
            serverSide: true,
            ajax: {
                url: "/company",
                type: 'GET',
            },
            columns: [
                    {data: 'id', name: 'id', 'visible': false},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email' },
                    {data: 'logo', name: 'logo' },
                    {data: 'website', name: 'website' },
                    {data: 'created_at', name: 'created_at' },
                    {data: 'updated_at', name: 'updated_at' },
                    {data: 'action', name: 'action', orderable: false},
                ],
            columnDefs:
                [{
                    "targets": 4,
                    "data": 'logo',
                    "render": function (data, type, row, meta) {
                        if(data){
                            return '<img src="' + '{{ URL::asset("storage/inventory") }}' + "/"+ data + '" alt="' + data + '"height="16" width="16"/>';
                        }else{
                            return data;
                        }
                        
                    }
                }],
            order: [[0, 'desc']],
            initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;

                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    });
                });
            }
        });

        /*  When user click add user button */
        $('#create-new-company').click(function () {
            $('#btn-save').val("create-product");
            $('#company_id').val('');
            $('#companyForm').trigger("reset");
            $('#companyCrudModal').html("Add New Company");
            $('#ajax-company-modal').modal('show');
        });

        $( "#companyForm" ).submit(function( event ) {
            $.ajax({
                // data: $('#companyForm').serialize(),
                data: new FormData(this),
                url: "company",
                type: "POST",
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#companyForm').trigger("reset");
                    $('#ajax-company-modal').modal('hide');
                    $('#btn-save').html('Save Changes');
                    var oTable = $('#table_company').dataTable();
                    oTable.fnDraw(false);
                    
                },
                error: function (data) {
                    if(data.status == 422){
                        if(data.responseJSON.errors.name){
                            $('#name_validation_message').html(data.responseJSON.errors.name[0]);
                        }
                        if(data.responseJSON.errors.logo){
                            $('#logo_validation_message').html(data.responseJSON.errors.logo[0]);
                        }
                    }
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
            event.preventDefault();
        });

        /* When click edit user */
        $('body').on('click', '.edit-product', function () {
            var company_id = $(this).data('id');
            $.get('company/' + company_id +'/edit', function (data) {
                console.log('asd');
                // $('#title-error').hide();
                // $('#product_code-error').hide();
                // $('#description-error').hide();
                $('#companyCrudModal').html("Edit Company");
                $('#btn-save').val("edit-company");
                $('#ajax-company-modal').modal('show');
                $('#company_id').val(data.id);
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#website').val(data.website);
            })
        });

        $('body').on('click', '#delete-product', function () {
            var company_id = $(this).data("id");
            var get_csrf_token = $('#csrf_token').val();

            if(confirm("Are You sure want to delete !")){
                $.ajax({
                    type: "DELETE",
                    data: {
                        _token: get_csrf_token
                    },
                    url: "company/" + company_id,
                    success: function (data) {
                        var oTable = $('#table_company').dataTable(); 
                        oTable.fnDraw(false);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }); 
    });

    
</script>
    
@endsection
