@component('mail::message')
# Welcome {{ $company_name }}

Your company successfully registered


Thanks,<br>
{{ config('app.name') }}
@endcomponent
