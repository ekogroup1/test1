<?php

return [
    'first_name' => 'Nama Pertama',
    'last_name' => 'Nama Terakhir',
    'company' => 'Perusahaan',
    'created_at' => 'Dibuat pada',
    'updated_at' => 'Diupdate pada',
    'action' => 'Tindakan'
];