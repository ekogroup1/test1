<?php

return [
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'company' => 'Company',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'action' => 'Action'
];