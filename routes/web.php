<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\CompanyCreated;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/locale/{locale}', function($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Auth::routes(['register' => false]);

Route::resources([
    'company' => 'CompanyController',
    'employee' => 'EmployeeController'
]);

Route::get('/get-list-company', 'EmployeeController@getListCompany');



Route::get('/home', 'HomeController@index')->name('home');
